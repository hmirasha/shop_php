<?php

function getRecords($columns,$table,$wher,$order){
	global $conn;
	$sql="SELECT $columns FROM $table $wher $order";
	$stmt=$conn->prepare($sql);
	$stmt->execute();
	$row=$stmt->fetchAll();
    return $row;
}
function insertRecords($table,$columns,$values){
	global $conn;
	$sql="INSERT INTO $table $columns VALUES ($values)";
	$stmt=$conn->prepare($sql);
	$stmt->execute();
	if($stmt->rowCount()>0)
        return 1;
    else
    	return 0;
}
function updateRecords($table,$values,$wher){
	global $conn;
	$sql="UPDATE $table SET $values $wher";
	$stmt=$conn->prepare($sql);
	$stmt->execute();
	if($stmt->rowCount()>0)
        return 1;
    else
    	return 0;
}
function deleteRecord($table,$wher){
	global $conn;
	$sql="DELETE FROM $table $wher";
	$stmt=$conn->prepare($sql);
	$stmt->execute();
	if($stmt->rowCount()>0)
        return 1;
    else
    	return 0;
}
function showMessage($msg,$fail,$back){
	$pre=isset($back)&&($back!="")?$back:filter_var($_SERVER['HTTP_REFERER'], FILTER_VALIDATE_URL);
     $error=isset($fail)&&($fail!="")?'fail':'';
	if(is_array($msg)){
		echo "<div class='msg fail'>";
        foreach ($msg as $ms) {
       	echo $ms."<br>";
       }
       echo "</div>";
	}else{
		echo "<div class='msg ".$error."'>".$msg."</div>";
	}
	echo "<a href='".$pre."'>Back</a>";

}
