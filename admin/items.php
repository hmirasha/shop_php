<?php
session_start();
$pagetile='Members';
$navbar="";
include "init.php";
if(isset($_SESSION['username'])){
	$do=isset($_GET['do'])?$do=$_GET['do']:'manage';
	if($do=='manage'){
    $some="";
    if(isset($_GET['catid'])&&(!empty($_GET['catid']))&&is_numeric($_GET['catid'])){
      $some=" WHERE items.catid=".$_GET['catid'];
    }elseif (isset($_GET['userid'])&&(!empty($_GET['userid']))&&is_numeric($_GET['userid'])) {
      $some=" WHERE items.userid=".$_GET['userid'];
    }    
    $joi=" INNER JOIN categories ON categories.catid=items.catid INNER JOIN users ON users.userid=items.userid ";
    $wher=$joi.$some;
		$records=getRecords('*','items',$wher,'');
		?>
		<div class="container manage">
		<h1>Manage Items</h1>
    <div class="row moreOptions">
      |&nbsp;<a href="?do=add">Add new item</a>&nbsp;|
    </div>
		<table class="table table-hover text-center">
             <thead>
             	<tr>
                    <th>Name</th>
                    <th>Price</th>
                    <th>Category Name</th>
                    <th>User Name</th>
                    <th>Options</th>
                </tr>
             </thead>
             <tbody>
  	<?php
  	if(empty($records)){
  	echo "<tr>";
      echo "<td colspan='5'>No Items Selected</td>";
    echo "</tr>";
    }else{
    	foreach ($records as $rec) {
    			echo "<tr>";
                echo "<td><a href='?do=show&itemid=".$rec['itemid']."' class='btn btn-link'>".$rec['itemName']."</a></td>";
                echo "<td>".$rec['price']." <b>$</b></td>";
                echo "<td><a href='?do=manage&catid=".$rec['catid']."' class='someItems'>".$rec['catname'];
                echo "</a></td>";
                echo "<td><a href='?do=manage&userid=".$rec['userid']."' class='someItems'>".$rec['username'];
                echo "</a></td>";
                echo "<td>";
               if($rec['approve']==0){
                    echo "<a href='?do=approve&itemid=".$rec['itemid']."' class='btn btn-success optionEditors'>Approve</a> ";
                }
                    echo "<a href='?do=edit&itemid=".$rec['itemid']."' class='btn btn-primary optionEditors'>Edit</a> ";
                    echo "<a href='?do=delete&itemid=".$rec['itemid']."' class='btn btn-danger optionEditors'>Delete</a>";
                echo "</td>";
          echo "</tr>";
    	}
    }
  	?>
            </tbody>
        </table>			
		</div>
<?php
	}elseif($do=='add'){?>
		    <div class="adddiv container ">
      <h1>Add Item</h1>
      <form action="?do=insert" method="POST" class="layout" enctype="multipart/form-data">
      <div class="form-group row">
        <label class="col-sm-2 control-label">
          Name : 
        </label>
          <div class="col-sm-6">
            <input type="text" name="itemName" class="form-control" autocomplete="off" placeholder="Enter Name">
          </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 control-label">
          Description : 
        </label>
          <div class="col-sm-6">
            <textarea name="itemDescription" class="form-control" placeholder="Enter Description"></textarea> 
          </div>
      </div> 
      <div class="form-group row">
          <label class="col-sm-2  form-check-label">Category :</label>
        <div class="col-sm-6">    
         <select class="form-control form-block" name="catItem">
          <?php
            $cats=getRecords('*','categories','','');
            if(!empty($cats)){
                echo " <option value='0'>Select Category</option>";
                foreach ($cats as $cat) {
                  echo " <option value='".$cat['catid']."'>".$cat['catname']."</option>";
                }
            }else{
                  echo " <option value=''>No categories</option>";
                }
          ?>
        </select> 
        </div>
       </div>  

        <div class="form-group row">
          <label class="col-sm-2  form-check-label">User :</label>
        <div class="col-sm-6">    
         <select class="form-control form-block" name="itemUser">
          <?php
            $users=getRecords('*','users','','');
            if(!empty($users)){
                echo " <option value='0' Selected>Select User</option>";
                foreach ($users as $user) {
                  echo " <option value='".$user['userid']."'>".$user['username']."</option>";
                  }
            }else{
                  echo " <option value=''>No Users</option>";
                }
          ?>
        </select> 
        </div>
       </div> 

      <div class="form-group row">
        <label class="col-sm-2 control-label">
          Price : 
        </label>
          <div class="col-sm-6">
            <input type="text" name="itemPrice"  placeholder="..." autocomplete="off"><b>$</b>
      </div>
      </div> 

      <div class="form-group row">
          <label class="col-sm-2  form-check-label">Country Made :</label>
        <div class="col-sm-6">
            <input type="text" name="itemCountry" class="form-control" placeholder="Enter Country">
        </div>
       </div>

       <div class="form-group row">
        <label class="col-sm-2 control-label">
          Added Date : 
        </label>
          <div class="col-sm-6">
            <input type="date" name="itemDate" class="form-control" value="<?php echo $rows['addDate']; ?>">
          </div>
      </div>

       <div class="form-group row">
          <label class="col-sm-2  form-check-label">Image :</label>
        <div class="col-sm-6">
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="inputGroupFile01"
                    aria-describedby="inputGroupFileAddon01" name="itemImage">
                  <label class="custom-file-label" for="inputGroupFile01">...</label>
                </div>
              </div>
          </div>
         </div>

    <div class="form-group row">
      <label class="col-sm-2  form-check-label">Status :</label>
      <div class="col-sm-6">    
         <select class="form-control form-block" name="statusItem">
          <option value="0">Select Status</option>
          <option value="Excellent">Excellent</option>
          <option value="Good">Good</option>
          <option value="Not bad">Not bad</option>
          <option value="Bad">Bad</option>
          <option value="Used">Used</option>
         </select> 
        </div>
       </div>

        <div class="form-group row">
            <label class="col-sm-2  form-check-label">Rate :</label>
          <div class="col-sm-6">
              <input type="number" name="itemRate" min="1" max="5" value="1">
          </div>
       </div>
       
        <div class="form-group row ">
            <button type="submit" name="save" class="btn btn-info">Save</button>
        </div>
      </form>
      <hr>
       |<a href="?do=manage"> Back to Mange Items Page</a> |
    </div>
<?php
    }elseif($do=='insert'){
    	if($_SERVER['REQUEST_METHOD'] == 'POST'){
          if(!empty($_FILES['itemImage']['name'])){
          $itemImage=$_FILES['itemImage'];
          $itemImageName=$_FILES['itemImage']['name'];
          $itemImageSize=$_FILES['itemImage']['size'];
          $itemImageTmpName=$_FILES['itemImage']['tmp_name'];
          $itemImageType=$_FILES['itemImage']['type'];
          
          $allowedImageExtension= array("jpeg","jpg","png","gif");
          $extension=explode('.', $itemImageName); 
          $imageExtension=end($extension);
          }else{
            $itemImageName="";
          }
          $itemname=$_POST['itemName'];
          $itemDescription=filter_var($_POST['itemDescription'], FILTER_SANITIZE_STRING);
          $itemCat=$_POST['catItem'];
          $itemUser=$_POST['itemUser'];
          $itemPrice=filter_var($_POST['itemPrice'], FILTER_SANITIZE_NUMBER_INT);
          $itemCountry=filter_var($_POST['itemCountry'], FILTER_SANITIZE_STRING);
          $itemDate=$_POST['itemDate'];
          $statusItem=$_POST['statusItem'];
          $itemRate=$_POST['itemRate'];
          

    /*  
         if(empty($_FILES['itemImage'])){echo "yes";}
          echo $itemImageName."<br>";
          echo $itemImageSize."<br>";
          echo $itemImageTmpName."<br>";
          echo $itemImageType."<br>";
          echo $itemname."<br>";
          echo $itemDescription."<br>";
          echo $itemCat."<br>";
          echo $itemUser."<br>";
          echo $itemPrice."<br>";
          echo $itemCountry."<br>";
          echo $itemDate."<br>";
          echo $statusItem."<br>";
          echo $itemRate."<br>";
          echo $imageExtension."<br>";
        //  print_r(end($imageExtension));

         */


          $errors= array();
          if(empty($itemname))
          	$errors[]="Item Name Can't be <strong>Empty</strong>";
          if(strlen($itemname)<2)
          	$errors[]="Item Name Can't be less than <strong>2 chars</strong>";
          if(empty($itemPrice))
            $errors[]="Item Price Can't be <strong>Empty</strong>";
          if(!empty($_FILES['itemImage']['name'])&&$itemImageSize<1000)
            $errors[]="Item Image Size Can't be less than <strong>100M</strong>";
          if(!empty($_FILES['itemImage']['name'])&& !in_array($imageExtension, $allowedImageExtension))
            $errors[]="Item image extension not <strong>Allowed</strong>";
          if(empty($errors)){
            
            $imageName="";
            if(!empty($_FILES['itemImage']['name'])){
            $imageName=rand(0,10000)."_".$itemImageName;
            move_uploaded_file($itemImageTmpName, $imgs.$imageName);
            }
          	$newrecord="'". $itemname."','".$itemDescription."',".$itemPrice.",'".$itemDate."','".$itemCountry
                       ."','".$imageName."',".$itemRate.",'".$statusItem."',1,".$itemCat.",".$itemUser;
          	$insertRecord=insertRecords('items',
              '(itemName,itemDescription,price,addDate,countryMade,image,rate,status,approve,catid,userid)'
              ,$newrecord);
          	if($insertRecord==1)
          		showMessage("One Item inserted",'','');
          	else
          		showMessage("No Items Inserted",'error','?do=add');
            }else{
            	showMessage($errors,'','');
            }  
        }else
            header("Location:dashboard.php");
          // Edite Section
	}elseif($do=='edit'){
		  $itemid=$_GET['itemid'];
	    $stmt=$conn->prepare("SELECT * FROM items WHERE itemid=?");
     	$stmt->execute(array($itemid));
     	$rows=$stmt->fetch();
      if(!empty($rows)){
		?>
		<div class="adddiv container ">
      <h1>Edit Item</h1>
      <form action="?do=update" method="POST" class="layout" enctype="multipart/form-data">
      <div class="form-group row">
        <input type="hidden" name="itemid" value="<?php echo $rows['itemid']; ?>">
        <label class="col-sm-2 control-label">
          Name : 
        </label>
          <div class="col-sm-6">
            <input type="text" name="itemName" class="form-control" autocomplete="off" value="<?php echo $rows['itemName']; ?>">
            <input type="hidden" name="olditemName" class="form-control" autocomplete="off" value="<?php echo $rows['itemName']; ?>">
          </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 control-label">
          Description : 
        </label>
          <div class="col-sm-6">
            <textarea name="itemDescription" class="form-control"><?php echo $rows['itemDescription']; ?></textarea> 
          </div>
      </div> 
      <div class="form-group row">
          <label class="col-sm-2  form-check-label">Category :</label>
        <div class="col-sm-6">    
         <select class="form-control form-block" name="catItem">
          <?php
            $catid=$rows['itemid'];
            $cats=getRecords('*','categories','','');
            if(!empty($cats)){
                foreach ($cats as $cat) {
                  if($cat['catid']==$catid){
                  echo " <option value='".$cat['catid']."' Selected>".$cat['catname']."</option>";
                  }else{
                  echo " <option value='".$cat['catid']."'>".$cat['catname']."</option>";
                  }
                }
            }else{
                  echo " <option value=''>No categories</option>";
                }
          ?>
        </select> 
        </div>
       </div>  

        <div class="form-group row">
          <label class="col-sm-2  form-check-label">User :</label>
        <div class="col-sm-6">    
         <select class="form-control form-block" name="itemUser">
          <?php
            $userid=$rows['userid'];
            $users=getRecords('*','users','','');
            if(!empty($users)){
                foreach ($users as $user) {
                  if($user['userid']==$userid){
                  echo " <option value='".$user['userid']."' Selected>".$user['username']."</option>";
                  }else{
                  echo " <option value='".$user['userid']."'>".$user['username']."</option>";
                  }
                }
            }else{
                  echo " <option value=''>No User</option>";
                }
          ?>
        </select> 
        </div>
       </div> 

      <div class="form-group row">
        <label class="col-sm-2 control-label">
          Price : 
        </label>
          <div class="col-sm-6">
            <input type="text" name="itemPrice"  autocomplete="off" value="<?php echo $rows['price']; ?>"><b>$</b>
      </div>
      </div> 

      <div class="form-group row">
          <label class="col-sm-2  form-check-label">Country Made :</label>
        <div class="col-sm-6">
            <input type="text" name="itemCountry" class="form-control"
             value="<?php if(empty($rows['countryMade'])) echo "...";
                          else echo $rows['countryMade']; ?>">
        </div>
       </div>
       <div class="form-group row">
        <label class="col-sm-2 control-label">
          Added Date : 
        </label>
          <div class="col-sm-6">
            <input type="date" name="itemDate" class="form-control" value="<?php echo $rows['addDate']; ?>">
          </div>
      </div>

       <div class="form-group row">
          <label class="col-sm-2  form-check-label">Change image :</label>
        <div class="col-sm-6">
            <div class="input-group">
                <div class="input-group-prepend">
                  <span class="input-group-text" id="inputGroupFileAddon01">Upload</span>
                </div>
                <div class="custom-file">
                  <input type="file" class="custom-file-input" id="inputGroupFile01"
                    aria-describedby="inputGroupFileAddon01" name="itemImage">
                  <label class="custom-file-label" for="inputGroupFile01">...</label>
                  <input type="hidden" name="oldimage" value="<?php echo $rows['image']; ?>">
                </div>
              </div>
          </div>
         </div>

    <div class="form-group row">
      <label class="col-sm-2  form-check-label">Status :</label>
      <div class="col-sm-6">    
         <select class="form-control form-block" name="statusItem">
          <?php
             $State= $rows['status'];
          ?>
          <option value="Excellent" <?php if($State=="Excellent") echo "Selected"; ?>>Excellent</option>
          <option value="Good" <?php if($State=="Good") echo "Selected"; ?>>Good</option>
          <option value="Not bad" <?php if($State=="Not bad") echo "Selected"; ?>>Not bad</option>
          <option value="Bad" <?php if($State=="Bad") echo "Selected"; ?>>Bad</option>
          <option value="Used" <?php if($State=="Used") echo "Selected"; ?>>Used</option>
         </select> 
        </div>
       </div>

        <div class="form-group row">
          <label class="col-sm-2  form-check-label">Rate :</label>
        <div class="col-sm-6">
          <input type="number" name="itemRate" min="1" max="5" value="<?php echo $rows['rate'];?>">
        </div>
       </div>
       
        <div class="form-group row ">
            <button type="submit" name="save" class="btn btn-info">Save</button>
        </div>
      </form>
      <hr>
       |<a href="?do=manage"> Back to Mange Items Page</a> |
    </div>
<?php }else{
  showMessage("No Item Selected !!",'','?do=manage');
}
	}elseif($do=='update'){
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
          $itemid=$_POST['itemid'];
          if(!empty($_FILES['itemImage']['name'])){
	        $itemImage=$_FILES['itemImage'];
          $itemImageName=$_FILES['itemImage']['name'];
          $itemImageSize=$_FILES['itemImage']['size'];
          $itemImageTmpName=$_FILES['itemImage']['tmp_name'];
          $itemImageType=$_FILES['itemImage']['type'];
          $allowedImageExtension= array("jpeg","jpg","png","gif");
          $extension=explode('.', $itemImageName); 
          $imageExtension=end($extension);
          }

          $itemname=empty($_POST['itemName'])?$_POST['olditemName']:$_POST['itemName'];
          $itemDescription=filter_var($_POST['itemDescription'], FILTER_SANITIZE_STRING);
          $itemCat=$_POST['catItem'];
          $itemUser=$_POST['itemUser'];
          $itemPrice=filter_var($_POST['itemPrice'], FILTER_SANITIZE_NUMBER_INT);
          $itemCountry=filter_var($_POST['itemCountry'], FILTER_SANITIZE_STRING);
          $itemDate=$_POST['itemDate'];
          $statusItem=$_POST['statusItem'];
          $itemRate=$_POST['itemRate'];

        
          if(!empty($_FILES['itemImage']['name'])){
              echo $itemImageName."<br>";
              echo $itemImageSize."<br>";
              echo $itemImageTmpName."<br>";
          }

          echo $itemname."<br>";
          echo $itemDescription."<br>";
          echo $itemCat."<br>";
          echo $itemUser."<br>";
          echo $itemPrice."<br>";
          echo $itemCountry."<br>";
          echo $itemDate."<br>";
          echo $statusItem."<br>";
          echo $itemRate."<br>";
          echo $imageExtension."<br>";

          $errors= array();
          if(strlen($itemname)<2&&!empty($_POST['itemName']))
            $errors[]="Item name can't be less than <strong>2 chars</strong>";
          if(empty($itemPrice))
            $errors[]="Item price can't be <strong>Empty</strong>";
          if(!empty($_FILES['itemImage']['name'])&&$itemImageSize<1000)
            $errors[]="Item image size can't be less than <strong>100M</strong>";
          if(!empty($_FILES['itemImage']['name'])&& !in_array($imageExtension, $allowedImageExtension))
            $errors[]="Item image extension not <strong>Allowed</strong>";
          if(empty($errors)){

            $imageName="";
            if(!empty($_FILES['itemImage']['name'])){
              $imageName=rand(0,10000)."_".$itemImageName;
              move_uploaded_file($itemImageTmpName, $imgs.$imageName);
            }elseif(!empty($_FILES['itemImage'])) {
              $imageName=$_POST['olditemName'];
            }
            echo "image name   ".$imageName;
          /* $newrecord="itemName='".$username."',itemDescription='".$password."',price=".$email.",addDate='".$admin."',countryMade='".."',image='".."',rate="..",status='".."',approve=1,catid="..",userid=".;
          	$wher="WHERE userid=".$iduser;*/
          //	$updateRecord=updateRecords('users',$newrecord,$wher);
          	if($updateRecord==1)
          		showMessage("One record updated",'','');
          	else
          		showMessage("No records updated",'error','?do=add');
            }
            else{
            	showMessage($errors,'','');
            }  
        }else
            header("Location:dashboard.php");
  }elseif($do=='show'){
          $itemid=isset($_GET['itemid'])&&is_numeric($_GET['itemid'])?$_GET['itemid']:0;
          $stmt=$conn->prepare("SELECT * FROM items WHERE itemid=?");
          $stmt->execute(array($itemid));
          $row=$stmt->fetch();
          if(!empty($row)){
    ?>
       <div class="container itemsShow">
        <div class="media showItem">
          <img src="<?php echo $imgs.$row['image']; ?>" class="mr-3 text-center" alt=" ">
          <div class="media-body">
            <div class="row">
            <h4 class="col-sm-8 mt-0"><b><i><?php echo $row['itemName']; ?></i></b></h4>
            <div class="optinItem col-sm-4">
              <?php
              if($row['approve']==0){
                 echo "<a  class='apprIt' href='?do=approve&itemid=".$row['itemid']."'>Apprrove</a>&nbsp;||&nbsp;";
              }
              ?>
                <a href="?do=edit&itemid=<?php echo $row['itemid']; ?>"class="editItem">Edit</a>&nbsp;||&nbsp;
                <a href="?do=delete&itemid=<?php echo $row['itemid']; ?>">Delete</a>
            </div>
           
           </div>
           <hr> 
          <div class="container">
          <?php if (!empty($row['itemDescription'])){?>   
            <div class="row">
             <p><?php echo $row['itemDescription'];?></p>
            </div>
            <?php } ?>
              <div class="row">
                 <div class="col-sm-3">
                   <label class="infoItem">Price :</label>
                 </div>
                  <div class="col-sm-9">
                  <label><?php echo $row['price'];?><b>$</b></label> 
                 </div>
             </div>
            <div class="row">
                 <div class="col-sm-3">
                   <label class="infoItem">Date Added :</label>
                 </div>
                  <div class="col-sm-9">
                   <label><?php echo $row['addDate'];?></label>
                 </div>
             </div>
            <div class="row">
                 <div class="col-sm-3">
                   <label class="infoItem">Country Made :</label>
                 </div>
                  <div class="col-sm-9">  
                   <label><?php echo $row['countryMade'];?></label>
                 </div>
            </div>
            <div class="row">
                 <div class="col-sm-3">
                   <label class="infoItem">Category :</label>
                 </div>
                  <div class="col-sm-9">
                  <?php 
                    $stmt=$conn->prepare("SELECT catname FROM categories WHERE catid=?");
                    $stmt->execute(array($row['catid']));
                    $user=$stmt->fetch();
                     echo "<label>".$user['catname']."</label>";
                   ?>
                 </div>
             </div>
            <div class="row">
                 <div class="col-sm-3">
                   <label class="infoItem">User :</label>
                 </div>
                  <div class="col-sm-9">
                  <?php 
                    $stmt=$conn->prepare("SELECT username FROM users WHERE userid=?");
                    $stmt->execute(array($row['userid']));
                    $user=$stmt->fetch();
                     echo "<label>".$user['username']."</label>";
                   ?>
                 </div>
             </div>
            <div class="row">
                 <div class="col-sm-3">
                   <label class="infoItem">Status :</label>
                 </div>
                  <div class="col-sm-9">
                    <label><?php echo $row['status'];?></label>
                 </div>
             </div>
             <div class="row">
                 <div class="col-sm-3">
                   <label class="infoItem">Rate :</label>
                 </div>
                  <div class="col-sm-9">
                  <label><?php echo $row['rate'];?></label>
                 </div>
             </div>
       </div>
       </div>
       </div><hr>
        |<a href="?do=manage"> Back to Mange Items Page</a> |
      </div>
	<?php 
       }else{
        showMessage("No Item Selected",'error','?do=manage');
       }
       }elseif($do=='delete'){
	      $itemid=isset($_GET['itemid'])&&is_numeric($_GET['itemid'])?intval($_GET['itemid']):0;
	      $wher="WHERE 	itemid=".$itemid;
          	$deleteRecord=deleteRecord('items',$wher);
          	if($deleteRecord==1)
          		showMessage("One item deleted",'','');
          	else
          		showMessage("No Items Deleted",'error','?do=add'); 
	}elseif($do=='approve'){
		  $itemid=isset($_GET['itemid'])&&is_numeric($_GET['itemid'])?intval($_GET['itemid']):0;
      $value="approve = 1 ";
	    $wher="WHERE 	itemid=".$itemid;
          $approveRecord=updateRecords('items',$value,$wher);
          if($approveRecord==1)
          		showMessage("One item approved",'','');
          	else
          		showMessage("No  Items Approved",'error','?do=add'); 
    }
include $foot."footer.php";
}else{
	header("Location: index.php");
    exit();
}
