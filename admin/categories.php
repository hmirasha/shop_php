<?php
session_start();
$pagetile='Members';
$navbar="";
include "init.php";
if(isset($_SESSION['username'])){
	$do=isset($_GET['do'])?$do=$_GET['do']:'manage';
	if($do=='manage'){
		$records=getRecords('*','categories','','');
		?>
		<div class="container managecat">
      <h1>Manage Categories</h1>
      <div class="moreOptions">
          |&nbsp;<a href="?do=add">Add new category</a>&nbsp;|
      </div>
       <br>
      <!--start Cards Section-->
      <div id="accordion">
      <!--start Card Section-->
<?php if(empty($records)){?>
  <div class="card">
    <div class="card-header" id="headingThree">
         <label>No Categories Selected !!</label>
    </div>
    </div>
  
<?php }else{
         foreach ($records as $rec) {?>
            <div class="card">     
    <div class="card-header" id="headingThree">
      <h5 class="mb-0">
        <button class="btn btn-link collapsed" data-toggle="collapse" data-target="#collapse<?php echo $rec['catid'];?>" aria-expanded="false" aria-controls="collapseThree">
          <?php echo $rec['catname'];
           ?>
        </button>
        <div class="catOptions">
             <a href="items.php?catid=<?php echo $rec['catid'];?>">Show Items</a>          
        </div>
      </h5>
    </div>
    <div id="collapse<?php echo $rec['catid'];?>" class="collapse" class="collapse show" aria-labelledby="headingThree" data-parent="#accordion">
      <div class="card-body">
        <div class="catviscomadd">
        <?php 
         if($rec['visibility']==0||$rec['allowComments']==0||$rec['allowAds']==0){
          echo "| ";}
           if($rec['visibility']==0){
              echo "<label class='Invis'>Invisible</label> | ";
           }//else{
            //  echo"<label>Visible</label>";
           //}
           if($rec['allowComments']==0){
              echo"<label class='noComm'>Comments not allow</label> | ";
           }//else{
            //  echo"<label>Comments allow</label>";
          // }
           if($rec['allowAds']==0){
              echo"<label class='noAdds'> Adds not allow</label> | ";
          }//else{
           //   echo"<label>Adds allow</label>";
        // }
         echo "</div>";
          echo $rec['description'];
        ?>
        <div class="catEditeDelete">
          <a class="editcat" href="?do=edit&catid=<?php echo $rec['catid'];?>">Edit</a> | 
          <a class="deletecat" href="?do=delete&catid=<?php echo $rec['catid'];?>">Delete</a>
        </div>
      </div>
    </div>
    </div>  
<?php    }
    } //End else
         ?>
    <!--End Card Section-->
   </div>
      <!--End Cards Section-->
    </div>
<?php
	}elseif($do=='add'){?>
		<div class="adddiv container ">
			<h1>Add Member</h1>
			<form action="?do=insert" method="POST" class="layout">
			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Name : 
				</label>
			    <div class="col-sm-9">
			    	<input type="text" name="catName" class="form-control" autocomplete="off">
			    </div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Description : 
				</label>
			    <div class="col-sm-9">
			    	<textarea name="catDescription" class="form-control"></textarea> 
			    </div>
			</div>		
			 <div class="form-group row">
			    <label class="col-sm-2  form-check-label">Visibility :</label>
			    <input type="checkbox" name="catvisibility" value="1" class="col-md-5 offset-md-5 form-check-input" checked />
			 </div>
     
       <div class="form-group row">
          <label class="col-sm-2  form-check-label">Comments :</label>
          <input type="checkbox" name="catallowComments" value="1" class="col-md-5 offset-md-5 form-check-input" checked />
       </div>
       
      <div class="form-group row">
          <label class="col-sm-2  form-check-label">Adds :</label>
          <input type="checkbox" name="catallowAds" value="1" class="col-md-5 offset-md-5 form-check-input" checked />
       </div>
			  <div class="form-group row ">
			    	<button type="submit" name="save" class="btn btn-info">Save</button>
		  	</div>
			</form>
      <hr>
			 |<a href="?do=manage"> Back to Mange Category Page</a> |
		</div>

<?php
    }elseif($do=='insert'){
    	if($_SERVER['REQUEST_METHOD'] == 'POST'){
          $catname=$_POST['catName'];
          $catDescription=$_POST['catDescription'];
          $catvisibility=(isset($_POST['catvisibility'])&&($_POST['catvisibility']==1))?1:0;
          $catallowComments=(isset($_POST['catallowComments'])&&($_POST['catallowComments']==1))?1:0;
          $catallowAds=(isset($_POST['catallowAds'])&&($_POST['catallowAds']==1))?1:0;
          $errors= array();
          if(empty($catname))
          	$errors[]="Catergory Name Can't be <strong>Empty</strong>";
          if(strlen($catname)<2)
            $errors[]="Catergory Name Can't be less than <strong>3 chars</strong>";
          if(empty($errors)){
          	$newrecord="'". $catname."','".$catDescription."',".$catvisibility.",".$catallowComments.",".$catallowAds;
          	$insertRecord=insertRecords('categories','(catname,description,visibility,allowComments,allowAds)',$newrecord);
          	if($insertRecord==1)
          		showMessage("One record inserted",'','');
          	else
          		showMessage("No records inserted",'error','?do=add');
            }
            else{
            	showMessage($errors,'','');
            }  
        }else
            header("Location:dashboard.php");
	}elseif($do=='edit'){
	  	$idcat=$_GET['catid'];
	    $stmt=$conn->prepare("SELECT * FROM categories WHERE catid=?");
     	$stmt->execute(array($idcat));
     	$rows=$stmt->fetch();
      if(!empty($rows)){
		?>
		<div class="adddiv container ">
      <h1>Edit Member</h1>
      <form action="?do=update" method="POST" class="layout">
        <input type="hidden" name="idcat" value="<?php echo $rows['catid'];?>">
      <div class="form-group row">
        <label class="col-sm-2 control-label">
          Name : 
        </label>
          <div class="col-sm-9">
            <input type="text" name="catName" class="form-control" value="<?php echo $rows['catname'];?>" autocomplete="off">
          </div>
      </div>
      <div class="form-group row">
        <label class="col-sm-2 control-label">
          Description : 
        </label>
          <div class="col-sm-9">
            <textarea name="catDescription" class="form-control"><?php echo $rows['description'];?></textarea>
          </div>
      </div>    
       <div class="form-group row">
          <label class="col-sm-2  form-check-label">Visibility :</label>
          <?php if($rows['visibility']==1){?>          
          <input type="checkbox" name="catvisibility" value="1" class="col-md-5 offset-md-5 form-check-input" checked/>
        <?php }else{ ?>
          <input type="checkbox" name="catvisibility" value="1" class="col-md-5 offset-md-5 form-check-input" />
        <?php  } ?>
       </div>
     
       <div class="form-group row">
          <label class="col-sm-2  form-check-label">Comments :</label>
          <?php if($rows['allowComments']==1){?>          
           <input type="checkbox" name="catallowComments" value="1" class="col-md-5 offset-md-5 form-check-input" checked/>
        <?php }else{ ?>
          <input type="checkbox" name="catallowComments" value="1" class="col-md-5 offset-md-5 form-check-input" />
        <?php  } ?>
       </div>
       
      <div class="form-group row">
          <label class="col-sm-2  form-check-label">Adds :</label>
          <?php if($rows['allowAds']==1){?>          
           <input type="checkbox" name="catallowAds" value="1" class="col-md-5 offset-md-5 form-check-input" checked/>
          <?php }else{ ?>
          <input type="checkbox" name="catallowAds" value="1" class="col-md-5 offset-md-5 form-check-input" />
          <?php  } ?>
       </div>
        <div class="form-group row ">
            <button type="submit" name="save" class="btn btn-info">Save</button>
        </div>
      </form>
      <hr>
       |<a href="?do=manage"> Back to Mange Memeber Page</a> |
    </div>

<?php	
    }else{
      showMessage("One Catergory Selected !!!",'','');
    }
    }elseif($do=='update'){
      	if($_SERVER['REQUEST_METHOD'] == 'POST'){
	        $idcat=$_POST['idcat'];
          $catname=$_POST['catName'];
          $catDescription=$_POST['catDescription'];
          $catvisibility=(isset($_POST['catvisibility'])&&($_POST['catvisibility']==1))?1:0;
          $catallowComments=(isset($_POST['catallowComments'])&&($_POST['catallowComments']==1))?1:0;
          $catallowAds=(isset($_POST['catallowAds'])&&($_POST['catallowAds']==1))?1:0;
          $errors= array();
          if(empty($catname))
          	$errors[]="Category name can't be <strong>Empty</strong>";
          if(strlen($catname)<2)
            $errors[]="Category name can't be less than <strong>3 chars</strong>";
          if(empty($errors)){
          	$newrecord="catname='".$catname."',description='".$catDescription."',visibility=".$catvisibility.",allowComments=".$catallowComments.",allowAds=".$catallowAds;
          	$wher="WHERE catid=".$idcat;
            echo $newrecord.$wher;
          	$updateRecord=updateRecords('categories',$newrecord,$wher);
          	if($updateRecord==1)
          		showMessage("One category updated",'','');
          	else
          		showMessage("No category updated",'error','');
            }
            else{
            	showMessage($errors,'','');
            }  
        }else
            header("Location:dashboard.php");

	}elseif($do=='delete'){
	      $idcat=isset($_GET['catid'])&&is_numeric($_GET['catid'])?intval($_GET['catid']):0;
	      $wher="WHERE 	catid=".$idcat;
          	$deleteRecord=deleteRecord('categories',$wher);
          	if($deleteRecord==1)
          		showMessage("One category deleted",'','');
          	else
          		showMessage("No category deleted",'error',''); 
	}
include $foot."footer.php";
}else{
	header("Location: index.php");
    exit();
}
