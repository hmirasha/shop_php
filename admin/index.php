<?php 
session_start();
$pagetile="Login";
include "init.php";
if(isset($_SESSION['username'])){
   header("Location: dashboard.php");
}
if($_SERVER['REQUEST_METHOD']=="POST"){
   $username=$_POST['user'];
   $pass=$_POST['pass'];
   $stmt=$conn->prepare('SELECT * from users WHERE username=? AND password=? AND regstatus=1 AND adminsgroup=1');
   $stmt->execute(array($username,$pass));
   $row=$stmt->fetch();
   if($stmt->rowCount()>0){
    $_SESSION['userid']=$row['userid'];
    $_SESSION['username']=$username;
    header("Location: dashboard.php");
    exit();
    }
}else{
?>




<form class="login" action="<?php echo $_SERVER['PHP_SELF'];?>" method="post">
	<input class="form-control" type="text" name="user" placeholder="Enter UserName" autocomplete="off">
	<input class="form-control" type="Password" name="pass" placeholder="Enter Password" autocomplete="off">
    <button class="btn btn-primary btn-block" type="submit">Login</button>
</form>






<?php  }
 include $foot."footer.php";?>