<?php
session_start();
$pagetile='Members';
$navbar="";
include "init.php";
if(isset($_SESSION['username'])){
	$do=isset($_GET['do'])?$do=$_GET['do']:'manage';
	if($do=='manage'){
    $order="";
    $wher="";
    if(isset($_GET['orderby'])&&isset($_GET['ord'])){
      $col="";
    switch($_GET['orderby']) {
      case "Username":
         $col="username "; 
          break;
      case "Email":
        $col="email "; 
          break;
      case "Admin":
         $col="userid "; 
          break;
       }
      $orderdirect=$_GET['ord']=='ASC'?'ASC':'DESC';
      $order="ORDER BY ".$col.$orderdirect;
    }
    if(isset($_GET['admins'])){
    $admins=is_numeric($_GET['admins'])&&$_GET['admins']==1?1:0;
    $wher=$admins==1?' WHERE adminsgroup =1 ':' WHERE adminsgroup =0 ';
    }
   // echo $order;
//    echo $wher;
   $records=getRecords('*','users',$wher,$order);
		?>
		<div class="container manage">
		<h1>Manage Members</h1>
    <div class="row moreOptions">
      <a href="?do=add">Add new member</a>&nbsp;||&nbsp;
      <a href="?do=manage&admins=1">Admins</a>&nbsp;||&nbsp;
      <a href="?do=manage&admins=0">Only Memebers</a>
    </div>
		<table class="table table-hover text-center">
             <thead>
             	<tr>
                    <th scope="col"><a href="?<?php echo $_SERVER['QUERY_STRING'].'&'; ?>orderby=Username&ord=DESC">Username</a></th>
                    <th scope="col"><a href="?<?php echo $_SERVER['QUERY_STRING'].'&'; ?>orderby=Emai&ord=DESC">Email</a></th>
                    <th scope="col"><a href="?<?php echo $_SERVER['QUERY_STRING'].'&'; ?>orderby=Admin&ord=DESC">Admin</a></th>
                    <th scope="col">Options</th>
                </tr>
             </thead>
             <tbody>
  	<?php
  	if(empty($records)){
  	echo "<tr>";
      echo "<td colspan='3'>No Records Selected</td>";
    echo "</tr>";
    }else{
    	foreach ($records as $rec) {
    			echo "<tr>";
                echo "<td scope='col'>".$rec['username']."</td>";
                echo "<td scope='col'>".$rec['email']."</td>";
                if ($rec['adminsgroup']==1) {
                   	echo "<td scope='col'><i class='fas fa-user-tie fa-2x'></i></td>";
                }
                else{
                   echo "<td scope='col'><i class='fas fa-user fa-2x'></i></td>";	
                }
                echo "<td scope='col'>";
               if($rec['regstatus']!=1){
                    echo "<a href='?do=approve&userid=".$rec['userid']."' class='btn btn-success'>Active</a> ";
                }
               if($_SESSION['userid']==$rec['userid']|| $rec['adminsgroup']!=1){
                    echo "<a href='?do=edit&userid=".$rec['userid']."' class='btn btn-primary'>Edit</a> ";
                }
                if($rec['adminsgroup']!=1){
                    echo "<a href='?do=delete&userid=".$rec['userid']."' class='btn btn-danger'>Delete</a>";
                }
                echo "</td>";
                echo "</tr>";
    	}
    }
  	?>
            </tbody>
        </table>			
		</div>
<?php
	}elseif($do=='add'){?>
		<div class="adddiv container ">
			<h1>Add Member</h1>
			<form action="?do=insert" method="POST" class="layout">
			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Username : 
				</label>
			    <div class="col-sm-9">
			    	<input type="text" name="username" class="form-control" autocomplete="off">
			    </div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Password : 
				</label>
			    <div class="col-sm-9">
			    	<input type="password" name="pass" class="form-control" autocomplete="off">
			    </div>
			</div>				
			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Email : 
				</label>
			    <div class="col-sm-9">
			    	<input type="text" name="email" class="form-control" autocomplete="off">
			    </div>
			</div>
			 <div class="row">
			    <label class="col-sm-2  form-check-label">Admin :</label>
			    <input type="checkbox" name="admin" value="1" class="col-md-5 offset-md-5 form-check-input" />
			  </div>
			  <div class="form-group row ">
			    	<button type="submit" name="save" class="btn btn-info">Save</button>
			</div>
			</form>
      <hr>
			|<a href="?do=manage"> Back to Mange Memeber Page</a> |
		</div>

<?php
    }elseif($do=='insert'){
    	if($_SERVER['REQUEST_METHOD'] == 'POST'){
          $username=$_POST['username'];
          $password=$_POST['pass'];
          $email=$_POST['email'];
          $admin=(isset($_POST['admin'])&&($_POST['admin']==1))?1:0;
          $errors= array();
          if(empty($username))
          	$errors[]="Username Can't be <strong>Empty</strong>";
          if(empty($password))
          	$errors[]="Password Can't be <strong>Empty</strong>";
          if(empty($errors)){
          	$newrecord="'". $username."','".$password."','".$email."',1,".$admin;
          	$insertRecord=insertRecords('users','(username,password,email,regstatus,adminsgroup)',$newrecord);
          	if($insertRecord==1)
          		showMessage("One record inserted",'','');
          	else
          		showMessage("No records inserted",'error','?do=add');
            }
            else{
            	showMessage($errors,'','');
            }  
        }else
            header("Location:dashboard.php");
	}elseif($do=='edit'){
		$iduser=$_GET['userid'];
	    $stmt=$conn->prepare("SELECT * FROM users WHERE userid=?");
     	$stmt->execute(array($iduser));
     	$rows=$stmt->fetch();
		?>
		<div class="adddiv container ">
			<h1>Edit Member</h1>
			<form action="?do=update" method="POST" class="layout">
				<input type="hidden" name="userid" value="<?php echo $rows['userid'];?>">
			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Username : 
				</label>
			    <div class="col-sm-9">
			    	<input type="text" name="username"  value="<?php echo $rows['username'];?>" class="form-control" autocomplete="off">
			    </div>
			</div>
			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Password : 
				</label>
			    <div class="col-sm-9">
			    	<input type="Password" name="newpass" value="" class="form-control" autocomplete="off">
			    	<input type="hidden" name="oldpass" value="<?php echo $rows['password'];?>" class="form-control" autocomplete="off">
			    </div>
			</div>				
			<div class="form-group row">
				<label class="col-sm-2 control-label">
					Email : 
				</label>
			    <div class="col-sm-9">
			    	<input type="text" name="email" value="<?php echo $rows['email'];?>" class="form-control" autocomplete="off">
			    </div>
			</div>
			 <div class="row">
			    <label class="col-sm-2  form-check-label">Admin :</label>
			    <?php if($rows['adminsgroup']==1){?>
			    <input type="checkbox" name="admin" value="1" class="col-md-5 offset-md-5 form-check-input" checked/>
			    <?php
			    }else{ ?>
			    	<input type="checkbox" name="admin" value="1" class="col-md-5 offset-md-5 form-check-input"/>
			    <?php } ?>
			  </div>
			  <div class="form-group row ">
			    	<button type="submit" name="save" class="btn btn-info">Update</button>
			</div>
			</form>
			| <a href="?do=mange" >Back to Mange Memeber Page</a> |
		</div>

<?php	}elseif($do=='update'){
	if($_SERVER['REQUEST_METHOD'] == 'POST'){
	      $iduser=$_POST['userid'];
          $username=$_POST['username'];
          if(empty($_POST['newpass']))
             $password=$_POST['oldpass'];
          else
          $password=$_POST['newpass'];
          $email=$_POST['email'];
          $admin=(isset($_POST['admin'])&&($_POST['admin']==1))?1:0;
          $errors= array();
          if(empty($username))
          	$errors[]="Username Can't be <strong>Empty</strong>";
          if(empty($password))
          	$errors[]="Password Can't be <strong>Empty</strong>";
          if(empty($errors)){
          	$newrecord="username='".$username."',password='".$password."',email='".$email."',adminsgroup=".$admin;
          	$wher="WHERE userid=".$iduser;
          	$updateRecord=updateRecords('users',$newrecord,$wher);
          	if($updateRecord==1)
          		showMessage("One record updated",'','');
          	else
          		showMessage("No records updated",'error','?do=add');
            }
            else{
            	showMessage($errors,'','');
            }  
        }else
            header("Location:dashboard.php");

	}elseif($do=='delete'){
	      $iduser=isset($_GET['userid'])&&is_numeric($_GET['userid'])?intval($_GET['userid']):0;
	      $wher="WHERE 	userid=".$iduser;
          	$deleteRecord=deleteRecord('users',$wher);
          	if($deleteRecord==1)
          		showMessage("One record deleted",'','');
          	else
          		showMessage("No records deleted",'error','?do=add'); 
	}elseif($do=='approve'){
		 $iduser=isset($_GET['userid'])&&is_numeric($_GET['userid'])?intval($_GET['userid']):0;
          $value="regstatus = 1 ";
	      $wher="WHERE 	userid=".$iduser;
          $approveRecord=updateRecords('users',$value,$wher);
          if($approveRecord==1)
          		showMessage("One user approved",'','');
          	else
          		showMessage("No  user approved",'error','?do=add'); 
    }
include $foot."footer.php";
}else{
	header("Location: index.php");
    exit();
}
